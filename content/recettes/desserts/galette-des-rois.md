---
title: "Galette Des Rois"
date: 2018-01-06T11:14:49+01:00
ingrédients:
- produit: "pâtes feuilletés (pur beurre)"
  quantité: 2
- produit: "œufs"
  quantité: 2 (+1 jaune pour la finition)
- produit: "beurre"
  quantité: 100g
- produit: "sucre"
  quantité: 125g
- produit: "poudre d'amandes"
  quantité: 125g
- produit: "fruits (des cerises, des pommes, …)"
  quantité: optionnel
- produit: "fève"
  quantité: 1
temps:
  préparation: 10 min
  cuisson: 35 min
---


# Préparation

Pour la frangipane : on fait fondre le beurre, on rajoute le sucre, les 2 œufs et la poudre d'amande (éventuellement les fruits). On enferme la frangipane et la fève entre les deux pâtes feuilletées (on fait des rayures au couteau sur le dessus).

# Cuisson
35 min à 190°C (après 25 minutes on badigeonne avec un jaune d'œufs et du sucre mélangés).
