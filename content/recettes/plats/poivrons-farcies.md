---
title: "Poivrons farcies «Lelia Svetla»"
date: 2017-11-19T10:52:35+01:00
ingrédients:
  - produit: oignon
    quantité: 1 grosse bulbe
  - produit: carottes
    quantité: 2
  - produit: poivrons
    quantité: 6-7
  - produit: riz
    quantité: 1 verre
  - produit: tomates concassées
    quantité: 1 conserve
  - produit: veau haché
    quantité: 700g
  - produit: sel
  - produit: poivre
  - produit: persil
  - produit: paprika
  - produit: huile d'olive
  - produit: de farine
    quantité: 1,5 cuillères
  - produit: œuf
    quantité: 1
  - produit: yaourt
    quantité: 1 cuillère
temps:
  préparation: 40 min
  cuisson: 3O min
---

# Préparation

## la farce
On fait revenir dans une poile l'oignon + les restes des poivrerons décapuchés + le riz +la viande. On rajoute les tomates + sel + poivre + paprika + persil haché.
## les poivrons
On sale les poivrons de l'intérieur,  on les remplit avec la farce. On les range dans la casserole et on les saupoudre de farine et on met de l'eau jusqu'au bord des poivrons. On pose une assiette pardessus. On fait cuire 30 min.
## la sauce
Dans une casserole : la farine + l'œuf + le yaourt. On rajoute lentement la sauce de la cuisson en touillant. On chauffe, sans porter a l'ébullition.

