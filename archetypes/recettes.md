---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: false
ingrédients:
  - produit: bla
    quantité: XXX g
  - produit: bli
    quantité: YYY ml
temps:
  préparation: 10 min
  cuisson: 35 min
---

# Préparation
