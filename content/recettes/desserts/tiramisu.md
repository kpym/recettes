---
title: "Tiramisu"
date: 2017-11-19T11:04:05+01:00
ingrédients:
  - produit: mascarpone
    quantité: 500 g
  - produit: œufs
    quantité: 4
  - produit: cuillères a soupe de sucre
    quantité: 4
  - produit: paquet de boudoirs
    quantité: 1
  - produit: café
  - produit: lait
  - produit: cognac
  - produit: cacao (Van Houten)
temps:
  préparation: 30
---

# Préparation

- Le mascarpone + 4 jaunes d'œuf + le sucre + les blancs battus en neige = la crème.
- café au lait + cognac et on trempe les boudoirs dedans. On pose : 1/2 des boudoirs + 1/3 de la crème +  le reste des boudoirs + le reste de la crème + le cacao en poudre. Dans le frigo pendant quelques heurs. Il faut le sortir 1/2 heur avant la consommation.

# Note
- Ne pas trop tremper les boudoirs sinon il reste du liquide au fond.
- Pour l'été on peut remplacer le café au lait par un jus de framboise fraîchement presse (éventuellement au lait).

