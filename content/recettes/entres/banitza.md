---
title: "Баница"
date: 2017-11-18T17:11:15+01:00
draft: false
ingrédients:
  - produit: feuilles de brick
    quantité: 10
  - produit: yaourt
    quantité: 1 pot
  - produit: œufs
    quantité: 3
  - produit: fêta
    quantité: 200g,
  - produit: beurre
    quantité: ~100g
temps:
  préparation: 25
  cuisson: 35
---

# Préparation

On mélange le yaourt + 3 œufs + 200g de fêta => la sauce.
On fait fondre le beurre.
On met du beurre fondu sur chaque feuille de brick suivie de la sauce et on enroule.
Au four 35 min à 190°C.
