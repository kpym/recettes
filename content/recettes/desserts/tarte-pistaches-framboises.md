---
title: "Tarte Pistaches Framboises"
date: 2017-11-19T12:22:29+01:00
draft: false
ingrédients:
  - section: pour la pâte sablé
  - produit: beurre
    quantité: 100g
  - produit: sucre
    quantité: 100g
  - produit: farine
    quantité: 200g
  - produit: jaune d'œuf
    quantité: 1
  - section: pour la ganache
  - produit: beurre
    quantité: 80g
  - produit: sucre
    quantité: 100g
  - produit: œufs
    quantité: 3 (ou 2 + 1 blanc)
  - produit: pistaches (ou 50/50 avec amandes) en poudre
    quantité: 100g
  - produit: farine
    quantité: 20-30g
  - section : fruits
  - produit: framboises
    quantité: 300g
temps:
  préparation: 30
  cuisson: 35
---

# Préparation

## la pâte sablé
  - 30 sec le beurre dans le micro-onde (pour devenir souple)
  - on rajoute le sucre, on mélange
  - on rajoute le jaune d'œuf, on mélange
  - on rajoute la farine, on mélange avec la cuillère : ça fait une pâte semblable à du «crumble»
  - on dispose dans le plat en distribuant avec la cuillère
  - on cuit 11 min à 190°C

## la ganache
  - 1 min le beurre dans le micro-onde (pour devenir liquide)
  - on rajoute le sucre, on mélange
  - on rajoute les œufs, on mélange
  - on rajoute la pistache, on mélange
  - on verse sur la pâte sablé chaude
  - on cuit 14 min à 190°C

## les fruit
  - on dispose les framboises (surgelés) sur la ganache chaude
  - on cuit encode 10 min (5 min si les framboises sont fraîches)

# Cuisson

À 190°C : 11 min la pâte + 14 min avec la ganache + 10 min avec les fruits

# Remarque
Il semble qu'il existe une recette dans le livre «Les tartes d'Eric Kayser».

