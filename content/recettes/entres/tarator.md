---
title: "Таратор"
date: 2017-11-18T17:52:58+01:00
draft: false
ingrédients:
  - produit: yaourts
    quantité: 4
  - produit: concombre
    quantité: 1
  - produit: gousse d'ail
    quantité: 1
  - produit: noix
    quantité: 30 g
  - produit: lait
    quantité: 100 ml
  - produit: sel
    quantité: 1 mg
  - produit: aneth
    quantité: 1/2 botte
  - produit: huile d'olives
    quantité: 3 cuillères
temps:
  préparation: 25
---

# Préparation

On écrase l'ail avec le sel dans le хаванче. On rajoute le concombre coupé en petits cubes et on le bat avec l'ail écrasé. On le mélange avec le reste (les noix écrasées, le lait selon les goûts, pas trop de l'huile d'olives).
