---
title: "Кюфтета"
date: 2017-11-19T10:58:13+01:00
ingrédients:
  - produit: blanquette veau haché
    quantité: 500g
  - produit: oignon
    quantité: 1
  - produit: poivron
    quantité: 1
  - produit: œufs
    quantité: 2
  - produit: tranche de pain
    quantité: 1
  - produit: lait
    quantité: éventuellement un peu
  - produit: sel
  - produit: poivre
  - produit: persil
  - produit: джоджан
  - produit: farine
  - produit: huile
temps:
  préparation: 20 min
  cuisson: 35 min
---

# Préparation

On trempe le pain dans du lait ou de l'eau. On coupe finement le poivron. Idem pour le persil. On mélange tout (excepté la farine et l'huile). Si le mélange est trop sec on rajoute un peu d'eau. On fait des grosses boulettes qu'on roule dans la farine et on les fait cuire.
